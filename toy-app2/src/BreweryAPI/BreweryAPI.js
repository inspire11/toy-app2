import axios from 'axios';


const getBreweries = async (page) => {
    try {
        const response = await axios.get(`https://api.openbrewerydb.org/breweries?page=${page}&per_page=3`);
        console.log(response.data)
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

export { getBreweries };