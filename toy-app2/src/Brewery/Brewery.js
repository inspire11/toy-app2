import { React, useState, useEffect } from 'react';
import { getBreweries } from '../BreweryAPI/BreweryAPI';
import BreweryCard from './BreweryCard/BreweryCard';
import styles from './Brewery.module.scss';

const Brewery = () => {
  const [breweries, setBreweries] = useState([]);
  const [page, setPage] = useState(1);

  const onClickHandler = (buttonName) => {
    if (buttonName ==='previous' ) {
      if (page > 0) {
        setPage(page - 1);
      }
    } else if (buttonName === 'next') {
      setPage(page + 1); 
    }
  }

  useEffect(() => {
    const getBrewery = async (page) => {
      const response = await getBreweries(page);
      const aggregatedBrewery = response.map(brewery => {
        let breweryObj = {}
        breweryObj["name"] = brewery.name
        breweryObj["id"] = brewery.id
        breweryObj["type"] = brewery.brewery_type
        return breweryObj;
      });
      setBreweries(aggregatedBrewery) 
      return aggregatedBrewery;
    }
   getBrewery(page);
  }, [page, breweries]);

  return (
        <div className={styles.breweryContainer}>
          <div className={styles.breweriesContainer}>
            {breweries.map((brewery, key) => {
              return (
              <div className={styles.breweryCardContainer} key={key}>
                  <BreweryCard
                  breweryName={brewery.name}
                  breweryId={brewery.id}
                  breweryType={brewery.type}
                />
                  
              </div>
              )
            })}
         </div>
        <div className={styles.buttonContainer}>
          <button
            className={styles.button}
            onClick={() => onClickHandler('previous')}
          >
            Previous
          </button>
          <button
            className={styles.button}
            onClick={() => onClickHandler('next')}
          >
            Next
          </button>
        </div>
            
       
      </div>
    )
}

export default Brewery;