import React from 'react';
import styles from './BreweryCard.module.scss';

const BreweryCard = ({
    breweryName,
    breweryId,
    breweryType
}) => {
    
    return (
        <div className={styles.breweryCardContainer}>
            <div className={styles.header}>
                <div className={styles.breweryName}>
                    Name: {breweryName}
                </div>
                <div className={styles.breweryId}>
                    Id: {breweryId}
                </div>
            </div>
            <div className={styles.body}>
                <div className={styles.breweryType}>
                    Type: {breweryType}
                </div>
            </div>
        </div>
    )
}

export default BreweryCard;