import React from 'react';
import Brewery from './Brewery/Brewery';
import styles from './App.css';

const App = () => {

  return (
    <div className={styles.main}>
        <Brewery />
    </div>
  )
}

export default App;