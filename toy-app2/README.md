## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Three cards should populate with a Brewery Name, Id and Type of Brewery from the Brewery API
When the user clicks on the 'Next' button, the next set of cards will be shown
When user clicks on the 'Previous' button, the previous set of cards will be shown

### `npm run test`

Launches the react library testing suite

